#include <iostream>
#include <string>
#include "list.h"
using namespace std;

void List::headPush(student var) {

	Node *tmp;

	if (tail == 0 && head == 0) { //fist push
		head = new Node(var, 0, 0);
		tail = head;
	}
	else {
		tmp = new Node(var, head, 0);

		head->prev = tmp;
		head = tmp;
	}

}

void List::tailPush(student var) {

	Node *tmp;

	if (tail == 0 && head == 0) { //fist push
		tail = new Node(var, 0, tail); // Don't need tmp ;)
		head = tail;
	}
	else {
		tmp = new Node(var, 0, tail); // Don't need tmp ;)
		tail->next = tmp;
		tail = tmp;
	}

}

bool List::headPop() {

	if (isEmpty()) {
		cout << "Link List Is Empty! ";
		return false;
	}

	Node *tmp = head->next;

	delete head;

	head = tmp;

	head->prev = 0;

	return true;
}

bool List::tailPop() {

	if (isEmpty()) {
		cout << "Link List Is Empty! ";
		return true;
	}

	Node *tmp;

	tmp = tail->prev;

	delete tail;

	tail = tmp;

	tmp->next = 0;

	return true;

}

student List::get() {

	Node *tmp = head;

	for (int i = 0; i < this->skipvalue ; i++) {
		if (tmp->next == NULL) return tmp->info;
		tmp = tmp->next; // next pointer
	}

	return tmp->info;
}

List::~List() {
	for (Node *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

List& List_table :: operator [](string key) {

	int value = 0;
	int index;
	for (int i = 0; i < key.length(); i++) {
		value += key[i];
	}

	index = value % this->amount;

	table[index].skip(value / this->amount);

	return this->table[index];
}

List& List_table :: operator [](int value) {

	int index;
	index = value % this->amount;

	table[index].skip(value / this->amount);

	return this->table[index];
}