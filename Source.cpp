#include <iostream>
#include <string>
#include "list.h"
using namespace std;

const int TABLE_SIZE = 50; // amount student

void main() {

	List_table table(TABLE_SIZE);
	
	student a;

	int i;
	for (i = 0; i <= 60; i++) {
		a.id = i;
		a.name = "bot_"+i;
		table[i] = a;
	}


	a.id = 4; a.name = "d";
	table["santi"] = a;

	a.id = 5; a.name = "karn patanukom";
	table["karn"] = a;

	a.id = 6; a.name = "ken cosh!";
	table["ken"] = a;

	cout << "info = " << table[1].get().name << "(" << table[1].get().id << ")" << endl;
	cout << "info = " << table["ken"].get().name << "(" << table["ken"].get().id << ")" << endl;
	cout << "info = " << table["karn"].get().name << "(" << table["karn"].get().id << ")" << endl;
	cout << "info = " << table[50].get().name << "(" << table[50].get().id << ")" << endl;
	cout << "info = " << table[60].get().name << "(" << table[60].get().id << ")" << endl;

	system("pause");

}