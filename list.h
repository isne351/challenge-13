#ifndef LIST
#define LIST
using namespace std;

struct student {
	int id;
	string name;
};

class Node {
public:
	student info;
	Node *next, *prev;
	Node() { next = prev = 0; }
	Node(student el, Node *n = 0, Node *p = 0) { info = el; next = n; prev = p; }
};

class List {
public:
	List() { head = tail = 0; }
	~List();
	bool isEmpty() { return head == 0; }

	bool headPop(); //Remove and return element from front of list
	bool tailPop(); //Remove and return element from tail of list

	void Unique();//remove duplicate element
	void Sort();//sort element

	void operator=(student input) { tailPush(input); }
	void skip(int i) { skipvalue = i; }

	student get();

private:
	void headPush(student); //Add element to front of list
	void tailPush(student); //Add element to tail of list
	int skipvalue;
	Node *head, *tail;
};

class List_table {
public:
	List_table(int amount) { this->amount = amount;  this->table = new List[amount]; }
	List &operator[](string);
	List &operator[](int);
private:
	int amount;
	List *table;
};

#endif //LIST
